<?php

include 'config.php';

function out($message = '')
{
  fwrite(STDOUT, $message . "\n");
  flush();
}

function get_local_filename($photo_url)
{
  $filename = TMP_DIR . '/' . md5($photo_url);
  if (!file_exists($filename)) {
    file_put_contents($filename, file_get_contents($photo_url));
  }
  return $filename;
}

function get_jobs()
{
  $url = ANY_PRINTER_API_URL . '/brokers/' . BROKER_CLIENT_ID . '/jobs?client_secret=' . BROKER_CLIENT_SECRET;
  return json_decode(file_get_contents($url), true);
}

function job_done($job_id)
{
  $url  = ANY_PRINTER_API_URL . '/brokers/' . BROKER_CLIENT_ID . '/jobs/' . $job_id;
  $url .= '?client_secret=' . BROKER_CLIENT_SECRET;
  $url .= '&forceMethod=DELETE';
  $result = file_get_contents($url);
  error_log($result);
  return true;
}

function pull()
{
  foreach (get_jobs() as $id => $job) {
    if ($job['printer']['type'] == 'cups') {
      $printer_name = $job['printer']['system_name'];
      $filename = get_local_filename($job['photo_url']);
      cups_print_photo($printer_name, $filename);
      sleep(1);
      job_done($id);
      sleep(1);
    }
    elseif ($job['printer']['type'] == 'bluetooth') {
      $printer_name = $job['printer']['system_name'];
      $filename = get_local_filename($job['photo_url']);
      bluetooth_push_photo($printer_name, $filename);
      sleep(1);
      job_done($id);
      sleep(1);
    }
  }
}

function cups_print_photo($printer, $filename)
{
  $command = "lp -d {$printer} -o media=Postcard\(4x6in\) {$filename}";
  out($command);
  $result = shell_exec($command);
  out($result);
}

function bluetooth_push_photo($rfcomm, $filename)
{
  $command = "ussp-push /dev/{$rfcomm} {$filename} photo.jpg";
  out($command);
  $result = shell_exec($command);
  out($result);
}

$sleep = 1000000;
while (true) {
  out('Pull');
  pull();
  usleep($sleep);
}
